"use strict";

angular.module("product.module", [
    "product.services",
    "product.controllers"
])


.config(function($stateProvider){
    $stateProvider

    .state("tab.products", {
        url: '/products',
        views: {
            "tab-products": {
                templateUrl: 'templates/product/products.html',
                controller: 'ProductListController'
            }
        }
    })

    .state('tab.product-detail', {
      url: '/products/:productId',
      views: {
        'tab-products': {
          templateUrl: 'templates/product/product-detail.html',
          controller: 'ProductDetailController'
        }
      }
    })

    .state("tab.product-edit", {
        url: '/products/edit/:productId',
        views: {
            'tab-products': {
                templateUrl: 'templates/product/product-edit.html',
                controller: 'ProductEditController'
            }
        }
    })

    .state("tab.product-create",
    {
        url: '/products/create',
        views: {
            "tab-products": {
                 templateUrl: 'templates/product/product-edit.html',
                 controller: 'ProductEditController'
            }
        }
    })

})