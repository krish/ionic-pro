"use strict";

angular.module("product.services", [])
.service("productService", function($http){
    this.getProducts = function() {
         return $http({
           method: 'GET',
           url: 'http://localhost:3000/api/products'
        })
    }

    this.getProduct = function(id) {
        return $http.get("http://localhost:3000/api/products/"+id);
    }

    this.saveProduct = function(product) {
        if (product.id) { //update existing 
            //PUT /api/products/123
            return $http.put("http://localhost:3000/api/products/" + product.id, product);
        }

        //POST /api/products
        return $http.post("http://localhost:3000/api/products", product);
    }

    this.deleteProduct = function(id) {
           return $http.delete("http://localhost:3000/api/products/" + id);
    }
})

/*
$resource('/api/products/:id', null,{
    'update': { method:'PUT' }
});
*/

/*
.factory("Product", function($resource, config){
    console.log("My base url is ", config);
    return $resource(config.API_URL_PREFIX + "/api/products/:id");
})
.service("productService", function(Product){
    Product.query(function(data, header, status){
        console.log(data);
        console.log(header);
        console.log(status);
    })
})
*/