"use strict";

angular.module("product.controllers", [])

.controller("ProductListController", 
   function($scope, 
            $state,
            productService
            
            ){
       console.log("product list controller");
       
       /*productService.getProducts().then(function(response) {
           $scope.products = response.data;
       },
       function(errorResponse) {

       }
       );*/

 

  $scope.createProduct = function() {
      $state.go("tab.product-create");
  }

  $scope.refresh = function() {
       productService.getProducts().then(function(response) {
           $scope.products = response.data;
       });
  }

  $scope.deleteProduct = function(id){
      productService.deleteProduct(id)
      .then(function(response){
          console.log("product deleted");
          $scope.refresh();
      });
  }

  $scope.refresh();
})
.controller("ProductDetailController", function(
    $scope,
    $state,
    $stateParams,
    productService
){
    console.log("Product id is ", $stateParams.productId);
    productService.getProduct($stateParams.productId).then(function(response){
         $scope.product = response.data;
     })

     $scope.edit = function(id) {
         $state.go("tab.product-edit", 
         {'productId': id});
     }
})

.controller("ProductEditController", function(
    $scope,
    $stateParams,
    productService
){
    console.log("Edit ", $stateParams.productId);
    if ($stateParams.productId){
        productService.getProduct($stateParams.productId).then(function(response){
            $scope.product = response.data;
        });
    } else {
        $scope.product = {};
    }

    $scope.save = function() {
        console.log($scope.product);
        productService.saveProduct($scope.product)
        .then(function(response){
            console.log("data saved successfully", response.data);
            $scope.product = response.data;
        })
    }

})








/*





.controller("ProductListController2", function($scope, Product){
    //api/products
    Product.query(function(data, header){
        //console.log(data);
        $scope.products = data;
        var headers = header();
        console.log(headers);
        console.log(headers["content-type"]);
    })
})

.controller("ProductDetailController", function($scope, $state, $stateParams,  Product){
    console.log("product id is ", $stateParams.productId);

    $scope.product = Product.get({'id' : $stateParams.productId});

    $scope.edit = function() {
        console.log("State go");
        $state.go("tab.product-edit", {'productId': $scope.product.id})
    }
})

.controller("ProductEditController", function($scope, $stateParams, $ionicHistory, Product){
    console.log("product id in edit ", $stateParams.productId);  

     $scope.product = Product.get({'id' : $stateParams.productId});

    $scope.save = function() {
        console.log("save");

        $ionicHistory.goBack();
    }

})

;
*/