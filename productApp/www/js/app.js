angular.module('productApp', [
        'ionic', 
        "ngResource",
        "product.module"])

//.constant("BaseUrl", "http://localhost:3000")
.value("config", {
  API_URL_PREFIX: "http://localhost:3000"
})

.run(function(config){
  if (window.cordova) {
    config.API_URL_PREFIX = "http://10.0.3.2:3000";  
}
})

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider

  // setup an abstract state for the tabs directive
    .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })
 
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/tab/products');

});
