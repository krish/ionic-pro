=== Instruction ===

This repository contains two projects.

1. api-server
2. productApp


api-server has minimal REST API implementation provided by json-server.
All the data for API are stored inside
api-server/data

To run api server:

Command line to api-server

	npm install

to run with nodemon, nodemon automatically restart server on data change

	npm install nodemon -g
	nodemon

to run without nodemon,
	
	node index.js


DELETE/POST/PUT methods modify the data in the data folder. Backup is available in backup folder

productApp is an ionic app, with the following plugins. Make sure you install this plugins

	1. cordova plugin add https://github.com/phonegap/phonegap-plugin-barcodescanner.git
	2. cordova plugin add cordova-plugin-camera
	3. cordova plugin add https://github.com/litehelpers/Cordova-sqlite-storage.git

to work with ionic app, command line to productApp, 

	npm install
	ionic serve

To add platform for android
	
	ionic platform add android

to add ios support

	ionic platform add ios


